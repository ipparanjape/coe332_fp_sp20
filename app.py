import json, datetime, uuid
from flask import Flask, request, jsonify, render_template
from datetime import datetime
from hotqueue import HotQueue
import redis
from datetimerange import DateTimeRange

q = HotQueue("queue", host="127.0.0.1", port=6098, db=0)
rd = redis.StrictRedis(host="127.0.0.1", port=6098, db=0)

app = Flask(__name__)


with open('NYPD_Arrest_Data.txt') as json_file:
        data = json.load(json_file)


@app.route('/alldata',methods=['GET'])
def alldata():
	q.put(data)
	return data


## CHECKS CRIMES WITHIN A TIME RANGE
rangetimei = []
@app.route('/rangedates/<beginning>/<end>',methods=['GET'])
def rangedates(beginning = None, end = None):
        beginningtime = (datetime.strptime(beginning,'%m-%d-%Y'))
        endtime=(datetime.strptime(end,'%m-%d-%Y'))

        for i in range(0,len(data['Crimes'])):

                #converting time str from data to time type
                war_start = (((data['Crimes'])[i])['Arrest Date'])

                timei = (datetime.strptime(war_start, '%m/%d/%Y'))
                time_range = DateTimeRange(beginning, end)

                x = timei in time_range
                if x == True:
                        rangetimei.append((data['Crimes'])[i])
	
        q.put(jsonify(rangetimei))
        return jsonify(rangetimei)
        
        
## ADDS NEW ANIMAL
@app.route("/addnew",methods=['GET'])
def addnew():
        args = request.args
        Newcrime = {}

        if 'Arrest Date' in args:
                Arrest_Data = args['Arrest Date']
                Newcrime["Arrest Date"] = Arrest_Data

        if 'Borough' in args:
                Borought = args['Borough']
                Newcrime["Borough"] = Borough

        if 'Age Group' in args:
                Age_Group = args['Age Group']
                Newcrime["Age Group"] = Age_Group

        if 'Perpetrator Sex' in args:
                Perpetrator_Sex = args['Perpetrator Sex']
                Newcrime["Perpetrator Sex"] = Perpetrator_Sex

        if 'OFNS Description' in args:
                OFNS_Description = args['OFNS Description']
                Newcrime["OFNS Description"] = OFNS_Description

        if 'Latitude' in args:
                Latitude = args['Latitude']
                Newcrime["Latitude"] = Latitude

        if 'Longitude' in args:
                Longitude = args['Longitude']
                Newcrime["Longitude"] = Longitude

        data['Crimes'].append(Newcrime)
        q.put(Newcrime)
        return(Newcrime)
        


## Gets average
def Aver(nameo):
        return sum(nameo)/len(nameo)
## Checks the average longitude and latitude of crimes

lat = []
lon = []

@app.route("/centerofcrime",methods=['GET'])
def centerofcrime():
        for i in range(0,len(data['Crimes'])):
                latitudei = (((data['Crimes'])[i])['Latitude'])
                lat.append(latitudei)
                longitudei = (((data['Crimes'][i])['Longitude']))
                lon.append(longitudei)
        x = Aver(lat)
        y = Aver(lon)
        q.put(jsonify(x,y))
        return(jsonify(x,y))


#prints data by Perpetrator
@app.route("/perpetrator-sex",methods=['GET'])
def pepetratorsex():
        args = request.args
        if 'Perpetrator Sex' in args:
                PS = args['Perpetrator Sex']
        p_s = []
        for i in range(0,len(data['Crimes'])):
                if PS == (((data['Crimes'])[i])['Perpetrator Sex']):
                        p_s.append((((data['Crimes'])[i])))
        q.put(jsonify(p_s))
        return(jsonify(p_s))



@app.route("/crime_image",methods=['POST'])
def image():
        return redirect("https://bitbucket.org/ipparanjape/coe332_fp_sp20/src/master/COE332_FP/NYPD_CrimeMap.png")
    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')





