# COE 332 Final Project #

Team Members:

Ishan Paranjape (UT EID: ipp89)

Robert Hicks (UT EID: rh34853)

Josias Bahena (UT EID: jlb7879)

# Project Synopsis #

[Final Project Description](https://drive.google.com/file/d/1WdhoV1z6abL-5ncracp9EXsYy-CIFiYz/view?usp=sharing)

The New York City Police Department (NYPD) is known for being one of the most efficient crime-fighting police organizations in the world. Within just three months of 2020, NYPD has made over 40000 arrests and has publicized them for the world to see. Each arrest is tagged with an ID, and the description of the person being arrested is also revealed, such as their sex, age group, and race. Additionally, coordinates are provided for each place of arrest as is the borough/county in which it happened. 

[Image URL](https://bitbucket.org/ipparanjape/coe332_fp_sp20/src/master/COE332_FP/NYPD_CrimeMap.png)
