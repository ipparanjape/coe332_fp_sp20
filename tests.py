from app import app
import unittest

class BasicTests(unittest.TestCase):

   def setUp(self):
      self.app = app.test_client()
      self.app.testing = True
      pass

   def tearDown(self):
      pass

   def test_datapoint(self):
      response = self.app.get('/alldata')
      json_response = response.get_json()['Crimes'][1]['Borough']
