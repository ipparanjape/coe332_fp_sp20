from hotqueue import HotQueue
import redis
from flask import Flask
import json

q = HotQueue("queue", host="127.0.0.1", port=6108, db=0)
rd = redis.StrictRedis(host="127.0.0.1", port=6108, db=0)


@q.worker
def do_work(item):
	x = q.get(item)
	print(x)

do_work()

