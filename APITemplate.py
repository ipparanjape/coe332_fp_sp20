import json
from flask import Flask, request, jsonify
from datetime import datetime

app = Flask(__name__)

@app.route('/animal_heads/<index>', methods=['GET'])
def getTypeHeads(index):
    i = int(index)
    return get_data(i,"Head")

@app.route('/num_arms/<index>', methods=['GET'])
def getNumArms(index):
    i = int(index)
    return get_data(i,"No. Arms")

@app.route('/num_tails/<index>', methods=['GET'])
def getNumTails(index):
    i = int(index)
    return get_data(i,"No. Tails")

@app.route('/num_legs/<index>', methods=['GET'])
def getNumLegs(index):
    i = int(index)
    return get_data(i,"No. Legs")

@app.route('/create-animal', methods=['GET','POST'])
def animals():
    animal = request.args.get('animal')
    n_arms = request.args.get('narms')
    n_arms = int(n_arms)
    n_legs = request.args.get('nlegs')
    n_legs = int(n_legs)
    n_tails = request.args.get('ntails')
    n_tails = int(n_tails)
    list_animals = ['snake','bull','lion','raven','bunny']    
    if request.method == 'GET':
        if ((animal in list_animals) and (n_arms % (2) == 0 and n_arms
    	   <= (10)) and (n_legs % 3 == (0) and n_legs <= (15)) and 
    	   (n_tails == (1) or n_tails == (2))):
             return show_animal(animal, n_arms, n_legs, n_tails)
        else:
             return "This is not a valid animal\n"

    if request.method == 'POST':
        if ((animal in list_animals) and (n_arms % (2) == 0 and n_arms
           <= (10)) and (n_legs % 3 == (0) and n_legs <= (15)) and
           (n_tails == (1) or n_tails == (2))):
           return queue_animal(animal, n_arms, n_legs, n_tails)
        else:
           return "This is not a valid animal\n"

@app.route('/update-animal/<index>', methods = ['PUT'])
def update_creature(index):
    i = int(index)
    animal = request.args.get('animal')
    n_arms = request.args.get('narms')
    n_arms = int(n_arms)
    n_legs = request.args.get('nlegs')
    n_legs = int(n_legs)
    n_tails = request.args.get('ntails')
    n_tails = int(n_tails)
    list_animals = ['snake','bull','lion','raven','bunny']
    if ((animal in list_animals) and (n_arms % (2) == 0 and n_arms
           <= (10)) and (n_legs % 3 == (0) and n_legs <= (15)) and
           (n_tails == (1) or n_tails == (2))):
             return updated_animal(i, animal, n_arms, n_legs, n_tails)
    else:
        return 'This is not a valid animal\n'

@app.route('/delete-animal/<uuid>', methods=['DELETE'])
def remove_creature(uuid):
    return list_deletes(uuid)

@app.route('/delete-dates', methods=['DELETE'])
def rm_creature():
    m1 = request.args.get('m1') #month
    m1 = int(m1)
    d1 = request.args.get('d1') #day
    d1 = int(d1)
    m2 = request.args.get('m2') #month
    m2 = int(m2)
    d2 = request.args.get('d2') #day
    d2 = int(d2)

    return delete_dates(m1, d1, m2, d2)    

@app.route('/query-date', methods=['GET'])
def query():
    m1 = request.args.get('m1') #month
    m1 = int(m1)
    d1 = request.args.get('d1') #day
    d1 = int(d1)
    m2 = request.args.get('m2') #month
    m2 = int(m2)
    d2 = request.args.get('d2') #day
    d2 = int(d2)

    # Convert string to datetime    
    return convert_query(m1, d1, m2, d2)

@app.route('/return-head/<head_type>', methods=['GET'])
def show_heads(head_type):
     print(head_type)
     return specific_head(head_type)
    

@app.route('/mean-arms', methods=['GET'])
def avg_arms():
    return mean_arms()

@app.route('/mean-legs', methods=['GET'])
def avg_legs():
    return mean_legs()

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

def add_timestamp():
    cdate =  str(datetime.today())
    return cdate  

def add_id():
    eid = str(uuid.uuid4())
    return eid

def get_data(index,feature):
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      return json.dumps(data['animals'][index][feature]) 

def show_animal(animal, narms, nlegs, ntails):
    data = {'Head' : animal, 'No. Arms' : narms, 'No. Legs' : nlegs,
    	    'No. Tails' : ntails, 'Time Stamp:' : add_timestamp()}
    return data

def mean_arms():
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      total = 0
      for i in range(0, len(data['animals'])):
          total = total + int(json.dumps(data['animals'][i]['No. Arms']))
      return str(total/len(data['animals']))


def mean_legs():
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      total = 0
      for i in range(0, len(data['animals'])):
          total = total + int(json.dumps(data['animals'][i]['No. Legs']))
      return str(total/len(data['animals']))

def queue_animal(animal, narms, nlegs, ntails):
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      data['animals'].append({'Head' : animal, 'No. Arms' : narms, 'No. Legs' : nlegs,'No. Tails' : ntails, 'Time Stamp': add_timestamp(), 'Unique ID': add_id()})
      return data

def convert_query(m1,d1,m2,d2): # Assume all dates are in 2020 for simplicity
    querydate = {}
    querydate['animals'] = []
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      for i in range(0,len(data['animals'])):
          date_string = data['animals'][i]['Time Stamp']
          datetime_obj = datetime.strptime(date_string, '%Y-%m-%d')
          m = datetime_obj.month
          d = datetime_obj.day
          if(m >= m1 and m <= m2):
              if(d >= d1 and d <= d2):
                  querydate['animals'].append({
                  'Head' : data['animals'][i]['Head'],
                  'No. Arms' : data['animals'][i]['No. Arms'],
                  'No. Legs' : data['animals'][i]['No. Legs'],
                  'No. Tails' : data['animals'][i]['No. Tails'],
                  'Time Stamp' : data['animals'][i]['Time Stamp'],
                  'Unique ID' : data['animals'][i]['Unique ID']
                  })
    return querydate
            
def delete_dates(m1,d1,m2,d2): # Assume all dates are in 2020 for simplicity
    querydate = {}
    querydate['animals'] = []
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      for i in range(0,100):
          date_string = data['animals'][i]['Time Stamp']
          datetime_obj = datetime.strptime(date_string, '%Y-%m-%d')
          m = datetime_obj.month
          d = datetime_obj.day
          if(not(m >= m1 and m <= m2)):
              if(not(d >= d1 and d <= d2)):
                  querydate['animals'].append({
                  'Head' : data['animals'][i]['Head'],
                  'No. Arms' : data['animals'][i]['No. Arms'],
                  'No. Legs' : data['animals'][i]['No. Legs'],
                  'No. Tails' : data['animals'][i]['No. Tails'],
                  'Time Stamp' : data['animals'][i]['Time Stamp'],
                  'Unique ID' : data['animals'][i]['Unique ID']
                  })
    return querydate

def specific_head(h):
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      data2 = {}
      data2['animals'] = []
      for i in range(0,100):
          if(str(data['animals'][i]['Head']) == h):
              data2['animals'].append({
              'Head' : data['animals'][i]['Head'],
              'No. Arms' : data['animals'][i]['No. Arms'],
              'No. Legs' : data['animals'][i]['No. Legs'],
              'No. Tails' : data['animals'][i]['No. Tails'],
              'Time Stamp' : data['animals'][i]['Time Stamp'],
              'Unique ID' : data['animals'][i]['Unique ID']
              })
      return data2

def updated_animal(i, animal, n_arms, n_legs, n_tails):
    with open('DrMoreauMT.txt') as json_file:
      data = json.load(json_file)
      for a in range(0,len(data['animals'])):
          if(a == i):
             data['animals'][i]['Head'] = animal
             data['animals'][i]['No. Arms'] = n_arms
             data['animals'][i]['No. Legs'] = n_legs
             data['animals'][i]['No. Tails'] = n_tails
             data['animals'][i]['Time Stamp'] = add_timestamp()
    return data 

def list_deletes(uid):
    with open('DrMoreauMT.txt') as json_file:
      deleted = False
      data = json.load(json_file)
      for a in range(0,len(data['animals'])):
          if(str(data['animals'][a]['Unique ID']) == uid):
             deleted = True
             del data['animals'][a]
      if(deleted):
         return data
      else:
         return 'UUID does not exist.'
     
        
                      
